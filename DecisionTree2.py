import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
from sklearn import metrics
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import plot_tree
import matplotlib.pyplot as plt
from sklearn.tree import export_text
import warnings
warnings.filterwarnings('ignore')


PATH = 'Crop_recommendation.csv'
df = pd.read_csv(PATH)

# sns.heatmap(df.corr(),annot=True)
features = df[['N', 'P','K','temperature', 'humidity', 'ph', 'rainfall']]
target = df['label']
labels = df['label']
# Initializing empty lists to append all model's name and corresponding name
acc = []
model = []

# Splitting into train and test data
Xtrain, Xtest, Ytrain, Ytest = train_test_split(features,target,test_size = 0.2,random_state =2)
DecisionTree = DecisionTreeClassifier(criterion="entropy",random_state=2,max_depth=5)
DecisionTree.fit(Xtrain,Ytrain)

predicted_values = DecisionTree.predict(Xtest)
x = metrics.accuracy_score(Ytest, predicted_values)
acc.append(x)
model.append('Decision Tree')
print("DecisionTrees's Accuracy is: ", x*100)

print(classification_report(Ytest,predicted_values))

# Cross validation score (Decision Tree)
score = cross_val_score(DecisionTree, features, target,cv=5)
print('Score:',score)


#Test a prediction with provided data
data = np.array([[99,38,21,22.88330922,71.59722446,6.352471866,67.72777298]])
prediction = DecisionTree.predict(data)
print('Test Prediction:',prediction)



# /////////////////////////////#

# Visualize Decision Tree with larger font size
plt.figure(figsize=(20,10))
plot_tree(DecisionTree, feature_names=features.columns, class_names=DecisionTree.classes_, filled=True, fontsize=10, max_depth=3)
plt.show()


# Export Decision Tree as text with limited depth
tree_rules = export_text(DecisionTree, feature_names=features.columns)

# Replace class labels with crop names
for i, crop in enumerate(DecisionTree.classes_):
    tree_rules = tree_rules.replace(f"class: {i}", f"class: {crop}")

print(tree_rules)
